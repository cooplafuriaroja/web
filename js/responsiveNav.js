function responsiveNav() {
    /* Toma  el valor x por medio de identificar el ID*/
    var x = document.getElementById("icono");
    /* Corrobora si la clase es uno a la otra y la cambia */
    if (x.className === "icono") {
        x.className += "_responsivo";
    }
    else {
        x.className = "icono";
    }

    var y = document.getElementById("quienes_somos");
    if (y.className === "nav") {
        y.className += "_responsivo";
    }
    else {
        y.className = "nav";
    }

    var z = document.getElementById("trabajos");
    if (z.className === "nav") {
        z.className += "_responsivo";
    }
    else {
        z.className = "nav";
    }

    var h = document.getElementById("contacto");
    if (h.className === "nav") {
        h.className += "_responsivo";
    }
    else {
        h.className = "nav";
    }
}