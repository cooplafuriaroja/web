function abrirFoto(evt, abrirFoto) {
    var i, trabajos_fotos, trabajos_interior;

    trabajos_fotos = document.getElementsByClassName("trabajos_fotos");
    for (i = 0; i < trabajos_fotos.length; i++) 
    {
        trabajos_fotos[i].style.display = "none";
    }
    trabajos_interior = document.getElementsByClassName("trabajos_interior");
    for (i = 0; i < trabajos_interior.length; i++) 
    {
        trabajos_interior[i].className = trabajos_interior[i].className.replace(" active", "");
    }

    document.getElementById(abrirFoto).style.display = "block";
    evt.currentTarget.className += " active";
    }