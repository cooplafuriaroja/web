# Página web de la cooperativa.

Desarrollado integramente por los integrantes de la cooperativa.

Como primera página publicada queremos, ademas de mostrar nuestro trabajo,
que las demás personas puedan ver que servicios podemos brindar. Desde el
soporte a elementos de uso cotidiano, como PC, notebooks o tablet, hasta
el desarrollo de páginas web y hosteo en servidores propios y/o cooperativas amigas. En la medida
que nos organicemos mayores serán los alcances de soporte.

Creamos este espacio para la autogestión de nuestro trabajo y de esa manera complementar los conocimientos, experiencias y prácticas individuales. La cooperativa se desarrolla sobre la división de tareas, el aprendizaje conjunto, reuniones generales, decisiones en grupo y la organización horizontal. Creemos en el trabajo cooperativo como herramienta para garantizar un servicio acorde y efectivo a las necesidades de la sociedad. Tenemos el objetivo, encontrado al juntarnos, de que nuestro trabajo se enmarque dentro de la sinceridad y el respeto hacia los clientes que se demuestra en cada trabajo. 
